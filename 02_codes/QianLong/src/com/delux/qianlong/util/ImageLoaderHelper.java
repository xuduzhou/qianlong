package com.delux.qianlong.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.delux.qianlong.App;
import com.delux.qianlong.R;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * 控制ImageLoader类
 * 
 * @author xiongwen
 * 
 */
public class ImageLoaderHelper {

	private static final int MEMORY_CACHE = 5 * 1024 * 1024;
	private static final int DISK_CACHE_SIZE = 100 * 1024 * 1024;
	private static final int DISK_CACHE_COUNT = 300;

	private static ImageLoaderHelper instance;

	private ImageLoaderConfiguration config = null;

	private ImageLoaderHelper() {
	}

	public static synchronized ImageLoaderHelper getInstance() {
		if (instance == null) {
			instance = new ImageLoaderHelper();
		}
		return instance;
	}
	
	private DisplayImageOptions options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.default_loading)
		.showImageForEmptyUri(R.drawable.default_loading)
		.showImageOnFail(R.drawable.default_loading)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
        .build();
	
	/**
	 * 默认加载图片使用用户自定义 options
	 */
	public void displayImage(String imageUrl, ImageView imageView, DisplayImageOptions options) {
		config();
		if (options == null) {
			options = this.options;
		}
		getImageLoader().displayImage(imageUrl, imageView, options);
	}

	/**
	 * 默认加载图片 使用默认 options
	 */
	public void displayImage(String imageUrl, ImageView imageView) {
		config();
		getImageLoader().displayImage(imageUrl, imageView, options);
	}

	/**
	 * 默认加载图片，可以监听加载结果
	 */
	public void displayImage(String imageUrl, ImageView imageView, DisplayImageOptions options, ImageLoadingListener listener) {
		config();
		if (options == null) {
			options = this.options;
		}
		getImageLoader().displayImage(imageUrl, imageView, options, listener);

	}

	/**
	 * 使用默认config 加载 Bitmap
	 */
	public Bitmap loadImageSync(String imageUrl, ImageSize size, DisplayImageOptions options) {
		config();
		if (options == null) {
			options = this.options;
		}
		Bitmap bitmap = getImageLoader().loadImageSync(imageUrl, size, options);
		return bitmap;
	}

	/**
	 * 默认初始化
	 */
	private void config() {
		boolean inited = getImageLoader().isInited();
		if (inited) {
			return;
		}

		if (config == null) {
			config = new ImageLoaderConfiguration.Builder(App.getInstance())
			.memoryCacheExtraOptions(480, 800)
	        .diskCacheExtraOptions(480, 800, null)
            .denyCacheImageMultipleSizesInMemory()
            .memoryCache(new LruMemoryCache(MEMORY_CACHE))
            .memoryCacheSize(MEMORY_CACHE)
            .diskCacheSize(DISK_CACHE_SIZE)
            .diskCacheFileCount(DISK_CACHE_COUNT)
            .build();
		}

		getImageLoader().init(config);
	}

	/**
	 * 销毁
	 */
	public void clearCache() {
		try {
			getImageLoader().clearMemoryCache();
			getImageLoader().clearDiskCache();
		} catch (Exception e) {
		}
	}

	public ImageLoader getImageLoader() {
		return ImageLoader.getInstance();
	}

}
