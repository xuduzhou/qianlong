package com.delux.qianlong.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.delux.qianlong.AppConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class JSONUtil {
	private final static String TAG = AppConfig.DEBUG ? JSONUtil.class.getSimpleName() : null;
	private static Gson gson;
	private static Gson gsonNormal;

	/**
	 * JSONAnnotation
	 * 
	 * @return
	 */
	public static Gson getGson() {
		if (gson == null) {
			gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		}

		return gson;
	}

	public static Gson getGsonNormal() {
		if (gsonNormal == null) {
			gsonNormal = new GsonBuilder().create();
		}

		return gsonNormal;
	}

	public static <T> String toJSONString(T instance) {
		return getGson().toJson(instance);
	}

	/**
	 * 
	 * @param jsonStr
	 * @return Collection 该Collection中的item是map对象
	 */
	private static <T> Collection<T> instanceCollection(String jsonStr) {
		Collection<T> ts = getGson().fromJson(jsonStr, new TypeToken<Collection<T>>() {
		}.getType());
		return ts;
	}

	public static <T> T instance(String jsonStr, Class<T> cls) {
		T obj = getGson().fromJson(jsonStr, cls);
		return obj;
	}

	public static <T> void writeObjectCollection(Collection<T> objs, String persistencePath) {
		try {
			String str = getGson().toJson(objs, new TypeToken<Collection<T>>() {
			}.getType());
			writeFile(str, persistencePath);
		} catch (Exception e) {
			MLog.e(TAG, "Exceprion", e);
		}
	}

	public static void writeFile(String json, String persistencePath) {
		try {
			File f = new File(persistencePath);
			if (!f.exists()) {
				FileUtil.newFile(persistencePath);
			}

			FileUtil.writeToFile(persistencePath, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <T> void writeObjectArray(T[] objs, String persistencePath) {
		try {
			String str = getGson().toJson(objs, new TypeToken<T>() {
			}.getType());
			writeFile(str, persistencePath);
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}
	}

	public static <T> void writeObject(T obj, String persistencePath) {
		try {
			String str = getGson().toJson(obj);
			writeFile(str, persistencePath);
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}
	}

	public static <T> Collection<T> readCollection(String persistencePath, Collection<T> collection) {
		try {
			File f = new File(persistencePath);
			if (f.exists()) {
				String str = FileUtil.readFile(persistencePath);
				Collection<T> object = getGson().fromJson(str, new TypeToken<Collection<T>>() {
				}.getType());
				return object;
			}
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}

		return null;
	}

	public static <T> Collection<T> readCollection(String persistencePath) {
		try {
			File f = new File(persistencePath);
			if (f.exists()) {
				String str = FileUtil.readFile(persistencePath);
				Collection<T> object = null;
				object = instanceCollection(str);
				return object;
			}
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}

		return null;
	}

	public static <T> Object readObject(String persistencePath, Class<T> cls) {
		try {
			File f = new File(persistencePath);
			if (f.exists()) {
				String str = FileUtil.readFile(persistencePath);
				Object object = null;
				object = instance(str, cls);
				return object;
			}
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}

		return null;
	}

	public static <T> T instanceFromJSONMapObject(Map<String, Object> map, Class<T> cls) {
		T instance = null;
		try {
			instance = cls.newInstance();
			Field[] fields = cls.getFields();
			Set<String> keySet = map.keySet();
			for (int j = 0; j < fields.length; j++) {
				Field field = fields[j];
				if (keySet.contains(field.getName())) {
					if (!field.isAccessible()) {
						field.setAccessible(true);
					}
					Object value = map.get(field.getName());
					JSONUtil.copyFromValue(instance, value, field);
				}
			}
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}

		return instance;
	}

	public static void copyFromValue(Object instance, Object value, Field field) {
		try {
			// same class or subclass
			if (field.getType().equals(value.getClass()) || field.getType().isAssignableFrom(value.getClass())) {
				field.set(instance, value);
			} else if (value.getClass().equals(Double.class) || value.getClass().equals(double.class)) {
				Double d = (Double) value;
				if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
					value = d.intValue();
				} else if (field.getType().equals(Float.class) || field.getType().equals(float.class)) {
					value = d.floatValue();
				} else if (field.getType().equals(Long.class) || field.getType().equals(long.class)) {
					value = d.longValue();
				}

				field.set(instance, value);
			}
		} catch (Exception e) {
			MLog.e(TAG, "Exception", e);
		}
	}

}
