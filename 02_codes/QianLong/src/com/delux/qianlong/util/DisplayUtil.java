package com.delux.qianlong.util;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;

public class DisplayUtil {

	/**
	 * 是否横屏
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isLand(Context context) {
		return Configuration.ORIENTATION_LANDSCAPE == context.getResources().getConfiguration().orientation;
	}

	/**
	 * 是否是小米手机
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isMiPhone() {
		String str1 = Build.DISPLAY;
		if ((str1 != null) && (str1.toUpperCase().contains("MIUI"))) {
			return true;
		}

		String str2 = Build.MODEL;
		if ((str2 != null) && (str2.toUpperCase().contains("MI-ONE"))) {
			return true;
		}

		String str3 = Build.DEVICE;
		if ((str3 != null) && (str3.toUpperCase().contains("MIONE"))) {
			return true;
		}

		String str4 = Build.MANUFACTURER;
		if ((str4 != null) && (str4.toUpperCase().equalsIgnoreCase("XIAOMI"))) {
			return true;
		}

		String str5 = Build.PRODUCT;
		if ((str5 != null) && (str5.toUpperCase().contains("MIONE"))) {
			return true;
		}

		return false;
	}

	/**
	 * 是否是oppo手机
	 * 
	 * @return
	 */
	public static boolean isOppo() {
		String manufacturer = Build.MANUFACTURER;
		if (null != manufacturer && manufacturer.toUpperCase().contains("OPPO")) {
			return true;
		}
		return false;
	}

	public static boolean isSanXing() {
		String manufacturer = Build.MANUFACTURER;
		if (null != manufacturer && manufacturer.toLowerCase().contains("samsung")) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否有物理按键
	 * 
	 * @param context
	 *            上下文
	 * @return
	 */
	public static boolean hasPermanentMenuKey(Context context) {
		return ViewConfiguration.get(context).hasPermanentMenuKey();
	}

	/**
	 * 系统状态栏是否可见
	 */
	public static boolean isStatusBarVisible(Activity context) {
		WindowManager.LayoutParams params = context.getWindow().getAttributes();
		int paramsFlag = params.flags & (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
		if (paramsFlag == params.flags) {
			return true;
		}
		return false;
	}

	/**
	 * 隐藏系统状态栏
	 */
	public static void hideStatusBar(Activity context) {
		Window window = context.getWindow();

		// 该属性保证状态栏的隐藏和显示不会挤压Activity向下，状态栏覆盖在Activity之上
		// window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
		// WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);

		WindowManager.LayoutParams params = window.getAttributes();
		params.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
		window.setAttributes(params);
	}

	/**
	 * 显示系统状态栏
	 */
	public static void showStatusBar(Activity context) {
		Window window = context.getWindow();

		// 该属性保证状态栏的隐藏和显示不会挤压Activity向下，状态栏覆盖在Activity之上
		// window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
		// WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);

		WindowManager.LayoutParams params = window.getAttributes();
		params.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
		window.setAttributes(params);
	}

	/**
	 * 切换全屏状态
	 */
	public static void closeFullScreen(View root) {
		root.setSystemUiVisibility(0);
	}

	/**
	 * 打开全屏模式
	 */
	public static void openFullScreen(View root) {
		root.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
	}

	/**
	 * 切换系统状态栏状态
	 */
	public static void toggleStatusBar(Activity context) {
		if (isStatusBarVisible(context)) {
			hideStatusBar(context);
		} else {
			showStatusBar(context);
		}
	}

	/**
	 * 展开通知栏
	 * 
	 * @param context
	 */
	public static void showNotifications(Context context) {
		try {
			Object service = context.getSystemService("statusbar");
			Class<?> statusBarManager = Class.forName("android.app.StatusBarManager");
			Method expand = statusBarManager.getMethod("expand");
			expand.invoke(service);
		} catch (NoSuchMethodException e) {
			try {
				Object obj = context.getSystemService("statusbar");
				Class.forName("android.app.StatusBarManager").getMethod("expandNotificationsPanel", new Class[0]).invoke(obj, (Object[]) null);
			} catch (Exception e2) {
			}
		} catch (Exception e) {
		}
	}

	/**
	 * 收起通知栏
	 */
	public static void collapseStatusBar(Context context) {
		int currentApiVersion = android.os.Build.VERSION.SDK_INT;
		try {
			Object service = context.getSystemService("statusbar");
			Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
			Method collapse = null;
			if (service != null) {
				if (currentApiVersion <= 16) {
					collapse = statusbarManager.getMethod("collapse");
				} else {
					collapse = statusbarManager.getMethod("collapsePanels");
				}
				collapse.setAccessible(true);
				collapse.invoke(service);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getWindowHeight(Activity context) {
		if (null != context.getWindow().getDecorView()) {
			Rect rect = new Rect();
			context.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
			return rect.height();
		}
		return 0;
	}

	public static int getWindowWidth(Activity context) {
		if (null != context.getWindow().getDecorView()) {
			Rect rect = new Rect();
			context.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
			return rect.width();
		}
		return 0;
	}

	public static Rect getWindowRect(Activity context) {
		Rect rect = new Rect();
		if (null != context.getWindow().getDecorView()) {
			context.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
		}

		return rect;
	}

}
