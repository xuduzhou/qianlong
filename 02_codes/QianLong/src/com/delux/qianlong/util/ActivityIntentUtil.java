package com.delux.qianlong.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.delux.qianlong.activity.BaseActivity;

public class ActivityIntentUtil {

	/**
	 * activity跳转
	 * 
	 * @param context
	 *            上下文
	 * @param clz
	 *            跳转到clz
	 */
	public static void intent(Context context, Class<? extends BaseActivity> clz) {
		Intent intent = new Intent(context, clz);
		context.startActivity(intent);
	}

	public static void intent(Activity context, Class<? extends BaseActivity> clz, int requestCode) {
		Intent intent = new Intent(context, clz);
		context.startActivityForResult(intent, requestCode);
	}

	public static void intent(Context context, Intent intent) {
		context.startActivity(intent);
	}

	public static void intent(Activity context, Intent intent, int requestCode) {
		context.startActivityForResult(intent, requestCode);
	}

	public static void intentTabActivity(Context context, Class<? extends Activity> clz) {
		Intent intent = new Intent(context, clz);
		context.startActivity(intent);
	}

}
