package com.delux.qianlong.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5工具类
 */
public class Md5Util {

	/**
	 * 计算文件MD5值
	 * 
	 * @param file
	 *            计算file文件
	 * @return md5值
	 */
	public static String getFileMD5(File file) {
		if (!file.exists()) {
			return "no exists";
		} else if (file.isDirectory()) {
			return "is directory";
		}
		FileInputStream fis = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);
			byte[] buffer = new byte[8192];
			int length = -1;
			while ((length = fis.read(buffer)) != -1) {
				md.update(buffer, 0, length);
			}
			return bytesToString(md.digest());
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			try {
				if (null != fis) {
					fis.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * 计算文件MD5值
	 * 
	 * @param path
	 *            文件路径
	 * @return md5值
	 */
	public static String getFileMD5(String path) {
		File file = new File(path);
		return getFileMD5(file);
	}

	/**
	 * 转换bytes 为字符串
	 * 
	 * @param data
	 *            bytes
	 * @return String
	 */
	public static String bytesToString(byte[] data) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		char[] temp = new char[data.length * 2];
		for (int i = 0; i < data.length; i++) {
			byte b = data[i];
			temp[i * 2] = hexDigits[b >>> 4 & 0x0f];
			temp[i * 2 + 1] = hexDigits[b & 0x0f];
		}
		return new String(temp);
	}

	/**
	 * 计算字符串 MD5值
	 * 
	 * @param originString
	 *            待计算字符串
	 * @return MD5值
	 */
	public static String getStringMD5(String originString) {
		String resultString = null;
		try {
			resultString = new String(originString);
			MessageDigest md = MessageDigest.getInstance("MD5");
			resultString = bytesToString(md.digest(resultString.getBytes()));
		} catch (Exception ex) {
		}
		return resultString;
	}

}
