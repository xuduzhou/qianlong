package com.delux.qianlong.util;

/**
 * Comment:data transfer listener, use to listen the download or upload process.
 */
public interface DataTransferListener {

	/**
	 * start write file
	 */
	void started();

	/**
	 * the process
	 */
	void transferred(int length);

	/**
	 * complete the transfer
	 */
	void completed(String filePath);

	/**
	 * aborted
	 */
	void aborted();

	/**
	 * failed
	 */
	void failed();

	/**
	 * if the transfer is canceled
	 */
	boolean isCanceled(String filePath);

	public class DataTransferAdapter implements DataTransferListener {

		@Override
		public void started() {
		}

		@Override
		public void transferred(int length) {
		}

		@Override
		public void completed(String filePath) {
		}

		@Override
		public void aborted() {
		}

		@Override
		public void failed() {
		}

		@Override
		public boolean isCanceled(String filePath) {
			return false;
		}
	}
}
