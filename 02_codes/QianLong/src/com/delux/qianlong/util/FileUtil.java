package com.delux.qianlong.util;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.MediaColumns;

import com.delux.qianlong.AppConfig;

public class FileUtil {

	private final static String TAG = AppConfig.DEBUG ? FileUtil.class.getSimpleName() : null;
	private final static String tmpSuffix = ".tmp";

	private static final String ROOT_PATH = "/ClassLetter/";
	private static final String PHOTO_PATH = "Photo/";
	private static final String RECORD_PATH = "Record/";

	public static boolean isSDCardMounted() {
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) && FileUtil.isFileCanReadAndWrite(Environment.getExternalStorageDirectory().getAbsolutePath());
	}

	public static String getRootPath() throws IOErrorException {
		if (isSDCardMounted()) {
			return Environment.getExternalStorageDirectory().getPath() + ROOT_PATH;
		} else {
			throw new IOErrorException();
		}
	}

	public static String getPhotoFolderPath() throws IOErrorException {
		return getRootPath() + PHOTO_PATH;
	}

	public static String getRecordFolderPath() throws IOErrorException {
		return getRootPath() + RECORD_PATH;
	}

	public static File getPhotoFile() throws IOErrorException {
		File storageDir = new File(getRootPath() + PHOTO_PATH);
		createStorageDir(storageDir);
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "IMG_" + timeStamp + ".jpg";
		File file = new File(storageDir, imageFileName);
		try {
			file.createNewFile();
			return file;
		} catch (IOException e) {
			throw new IOErrorException();
		}
	}

	public static final String AAC = ".aac";

	public static String getRecordPath(String type) throws IOErrorException {
		File storageDir = new File(getRootPath() + RECORD_PATH);
		createStorageDir(storageDir);
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String fileName = "Record_" + timeStamp + type;
		File file = new File(storageDir, fileName);
		try {
			file.createNewFile();
			return file.getAbsolutePath();
		} catch (IOException e) {
			throw new IOErrorException();
		}
	}

	/**
	 * 创建目录
	 * 
	 * @param storageDir
	 * @return
	 * @throws IOErrorException
	 */
	public static File createStorageDir(File storageDir) throws IOErrorException {
		if (!storageDir.mkdirs() && !storageDir.exists()) {
			throw new IOErrorException();
		}
		return storageDir;
	}

	/**
	 * 将图片增加到相册
	 * 
	 * @param photoPath
	 * @param context
	 */
	public static void galleryAddPic(String photoPath, Context context) {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File f = new File(photoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		context.sendBroadcast(mediaScanIntent);
	}

	/**
	 * 是否可读可写
	 * 
	 * @param filePath
	 * @return
	 */
	public static boolean isFileCanReadAndWrite(String filePath) {
		if (null != filePath && filePath.length() > 0) {
			File f = new File(filePath);
			if (null != f && f.exists()) {
				return f.canRead() && f.canWrite();
			}
		}
		return false;
	}

	/**
	 * 修改文件权限
	 * 
	 * @param permission
	 * @param path
	 * @throws IOErrorException
	 */
	public static void chmod(String permission, String path) throws IOErrorException {
		try {
			String command = "chmod " + permission + " " + path;
			Runtime runtime = Runtime.getRuntime();
			runtime.exec(command);
		} catch (IOException e) {
			throw new IOErrorException();
		}
	}

	/**
	 * 移动文件到指定目录
	 * 
	 * @param oldPath
	 *            String 如：c:/fqf.txt
	 * @param newPath
	 *            String 如：d:/fqf.txt
	 */
	public static boolean moveFile(String oldPath, String newPath) {
		boolean result = false;
		if (oldPath != null && newPath != null) {
			File oldfile = new File(oldPath);
			if (oldfile.exists()) {
				File newFile = new File(newPath);
				// 先使用系统的renameTo，不成功再去手动拷贝
				if (oldfile.renameTo(newFile)) {
					result = true;
				} else if (copyFile(oldPath, newPath)) {
					result = delFile(oldPath);
					// 删除操作不成功，删除刚复制的文档
					if (!result) {
						delFile(newPath);
					}
				}
			}
		}
		return result;
	}

	/**
	 * 移动文件到指定目录
	 * 
	 * @param oldPath
	 *            String 如：c:/fqf.txt
	 * @param newPath
	 *            String 如：d:/fqf.txt
	 */
	public static void moveFolder(String oldPath, String newPath) {
		File oldfile = new File(oldPath);
		if (oldfile.exists()) {
			copyFolder(oldPath, newPath);
			delFolder(oldPath);
		}
	}

	private static final int IO_BUFFER_SIZE = 1024;

	public static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] b = new byte[IO_BUFFER_SIZE];
		int read;
		while ((read = in.read(b)) != -1) {
			out.write(b, 0, read);
		}
	}

	/**
	 * 复制单个文件
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：c:/fqf.txt
	 * @param newPath
	 *            String 复制后路径 如：f:/fqf.txt
	 * @return boolean
	 */
	public static boolean copyFile(String oldPath, String newPath) {
		File oldfile = new File(oldPath);
		if (!oldfile.exists())
			return false;

		boolean result = true;
		try {
			int byteread = 0;
			InputStream inStream = new FileInputStream(oldPath); // 读入原文件
			FileOutputStream fs = new FileOutputStream(newPath);
			byte[] buffer = new byte[1444];

			while ((byteread = inStream.read(buffer)) != -1) {
				fs.write(buffer, 0, byteread);
			}

			fs.close();
			inStream.close();
		} catch (Exception e) {
			System.out.println("复制单个文件操作出错");
			MLog.e(TAG, "Exception", e);
			result = false;
		}
		return result;
	}

	/**
	 * 复制单个文件，IO存盘时用，added by chenjieyu
	 * 
	 * @param in
	 *            InputStream 原文件流，template是放在assets中，找不到完整路径，要用InputStream
	 * @param destFile
	 *            复制后路径 如：c:/fqf.txt
	 */
	public static boolean copyFile(InputStream in, String destFile) {
		if (in == null || destFile == null)
			return false;

		File outFile = new File(destFile);
		if (outFile.exists()) {
			outFile.delete();
		}
		try {
			outFile.createNewFile();
			OutputStream out = new FileOutputStream(outFile);
			byte[] buf = new byte[IO_BUFFER_SIZE];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println("复制文件流操作出错");
			MLog.e(TAG, "Exception", e);
			return false;
		}
	}

	/**
	 * 复制整个文件夹内容
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：c:/fqf
	 * @param newPath
	 *            String 复制后路径 如：f:/fqf/ff
	 * @return boolean
	 */
	public static boolean copyFolder(String oldPath, String newPath) {

		try {
			(new File(newPath)).mkdirs(); // 如果文件夹不存在 则建立新文件夹
			File a = new File(oldPath);
			String[] file = a.list();
			File temp = null;
			for (int i = 0; i < file.length; i++) {
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}

				if (temp.isFile()) {
					FileInputStream input = new FileInputStream(temp);
					FileOutputStream output = new FileOutputStream(newPath + "/" + (temp.getName()).toString());
					byte[] b = new byte[1024 * 5];
					int len;
					while ((len = input.read(b)) != -1) {
						output.write(b, 0, len);
					}
					output.flush();
					output.close();
					input.close();
				}
				if (temp.isDirectory()) {// 如果是子文件夹
					copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
				}
			}
			return true;
		} catch (Exception e) {
			System.out.println("复制整个文件夹内容操作出错");
			MLog.e(TAG, "Exception", e);
			return false;
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param filePathAndName
	 *            String 文件路径及名称 如c:/fqf.txt
	 * @param fileContent
	 *            String
	 * @return boolean
	 */
	public static boolean delFile(String filePathAndName) {
		boolean result = false;
		if (filePathAndName == null)
			return false;

		try {
			java.io.File myDelFile = new java.io.File(filePathAndName);
			result = myDelFile.delete();
		} catch (Exception e) {
			MLog.e(TAG, "Exception for delFolder()", e);
			MLog.e(TAG, "Exception", e);
			result = false;
		}

		return result;
	}

	/**
	 * 删除文件夹
	 * 
	 * @param filePathAndName
	 *            String 文件夹路径及名称 如c:/fqf
	 * @param fileContent
	 *            String
	 * @return boolean
	 */
	public static boolean delFolder(String folderPath) {
		boolean result = false;
		if (folderPath == null) {
			return result;
		}
		try {
			delAllFile(folderPath); // 删除完里面所有内容
			java.io.File myFilePath = new java.io.File(folderPath);
			result = myFilePath.delete(); // 删除空文件夹
		} catch (Exception e) {
			MLog.e(TAG, "Exception for delFolder()", e);
			result = false;
		}

		return result;
	}

	/**
	 * 删除文件夹
	 * 
	 * @param filePathAndName
	 *            String 文件夹路径及名称 如c:/fqf
	 * @param fileContent
	 *            String
	 * @return boolean
	 */
	public static boolean delFolder(File folderFile) {
		boolean result = false;
		if (folderFile == null) {
			return result;
		}
		try {
			delAllFile(folderFile); // 删除完里面所有内容
			result = folderFile.delete(); // 删除空文件夹
		} catch (Exception e) {
			MLog.e(TAG, "Exception for delFolder()", e);
			result = false;
		}

		return result;
	}

	/**
	 * 删除文件夹里面的所有文件
	 * 
	 * @param path
	 *            文件夹路径 如 c:/fqf
	 */
	public static void delAllFile(String path) {
		if (!path.endsWith(File.separator)) {
			path += File.separator;
		}
		File file = new File(path);
		if (!file.exists()) {
			return;
		}
		if (!file.isDirectory()) {
			return;
		}
		String[] tempList = file.list();
		File temp = null;
		if (tempList != null) {
			for (int i = 0; i < tempList.length; i++) {
				temp = new File(path + tempList[i]);
				if (temp.isFile()) {
					temp.delete();
				} else if (temp.isDirectory()) {
					delAllFile(path + tempList[i]);// 先删除文件夹里面的文件
					delFolder(path + tempList[i]);// 再删除空文件夹
				}
			}
		}
	}

	public static void delAllFile(File file) {
		if (!file.exists()) {
			return;
		}
		if (!file.isDirectory()) {
			return;
		}
		File[] tempList = file.listFiles();
		File temp = null;
		if (tempList != null) {
			for (int i = 0; i < tempList.length; i++) {
				temp = tempList[i];
				if (temp.isFile()) {
					temp.delete();
				} else if (temp.isDirectory()) {
					delAllFile(tempList[i]);// 先删除文件夹里面的文件
					delFolder(tempList[i]);// 再删除空文件夹
				}
			}
		}
	}

	private static boolean isExpired(long modify, long expiredTime) {
		long now = new Date().getTime();
		return now - modify >= expiredTime;
	}

	/**
	 * 删除文件夹里面的所有过期文件文件,
	 * 
	 * @param expiredTime
	 *            过期时间，如1天 = 24*60*60*1000
	 */
	private static void delAllExpiredFile(File file, long expiredTime, boolean forceDeleteEmptyDir) {
		if (!file.exists()) {
			return;
		}
		if (!file.isDirectory()) {
			return;
		}
		File[] tempList = file.listFiles();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			temp = tempList[i];
			if (temp.isFile()) {
				if (isExpired(temp.lastModified(), expiredTime)) {
					temp.delete();
				}
			} else if (temp.isDirectory()) {
				delAllExpiredFile(tempList[i], expiredTime, forceDeleteEmptyDir);
			}
			deleteExpiredEmptyDir(temp, expiredTime, forceDeleteEmptyDir);
		}
	}

	public static void delAllOutDayFile(File file) {
		long expiredTime = 24 * 60 * 60 * 1000; // 一天
		delAllExpiredFile(file, expiredTime, true);
	}

	/**
	 * 删除文件夹里面的所有文件,一周以内的不删除
	 * 
	 * @param path
	 *            String 文件夹路径 如 c:/fqf
	 */
	public static void delAllExpiredFile(File file) {
		long expiredTime = 7 * 24 * 60 * 60 * 1000; // 一周
		delAllExpiredFile(file, expiredTime, false);
	}

	/**
	 * 循环删除空的过期文件夹
	 * 
	 * @param dir
	 * @param isForce
	 *            == true 强制删除，不管修改时间
	 */
	private static void deleteExpiredEmptyDir(File dir, long expiredTime, boolean isForce) {
		if (dir.isDirectory()) {
			File[] fs = dir.listFiles();
			if (fs != null && fs.length > 0) {
				for (int i = 0; i < fs.length; i++) {
					File tmpFile = fs[i];
					if (tmpFile.isDirectory()) {
						deleteExpiredEmptyDir(tmpFile, expiredTime, isForce);
					}
					if (tmpFile.isDirectory() && tmpFile.listFiles().length == 0 && (isExpired(tmpFile.lastModified(), expiredTime) || isForce)) {
						tmpFile.delete();
					}
				}
			}
			if (dir.isDirectory() && dir.listFiles().length == 0 && (isExpired(dir.lastModified(), expiredTime) || isForce)) {
				dir.delete();
			}
		}
	}

	public static boolean addToFile(String filePath, InputStream io, long size, long startIndex, DataTransferListener listener) throws IOException {
		RandomAccessFile out = new RandomAccessFile(filePath, "rw");
		try {
			if (listener != null)
				listener.started();

			out.seek(startIndex);
			long downloadSize = startIndex;
			int nRead = 0;
			byte[] b = new byte[1024 * 64];
			while ((nRead = io.read(b, 0, 1024)) != -1) {
				if (listener == null) {
					out.write(b, 0, nRead);
					downloadSize += nRead;
				} else {
					if (listener.isCanceled(filePath)) {
						listener.aborted();
						break;
					} else {
						out.write(b, 0, nRead);
						downloadSize += nRead;
						listener.transferred((int) (downloadSize * 100 / size));
					}
				}
			}
		} finally {
			closeRandomAccessFile(out);
		}
		return true;
	}

	/**
	 * 将流写入文件
	 * 
	 * @param filePath
	 * @param io
	 * @param size
	 *            文件大小
	 * @param startIndex
	 *            写文件的位置
	 * @param listener
	 *            进度监听
	 * @param isUserTmpFile
	 *            true(先写入到tmp文件， 完成后把tmp文件rename成正式文件)
	 * @return
	 * @throws IOException
	 */
	public static boolean writeToFile(String filePath, InputStream io, long size, long startIndex, boolean isUserTmpFile, DataTransferListener listener) throws IOException {
		addToFile(filePath + tmpSuffix, io, size, startIndex, listener);

		if (listener != null && !listener.isCanceled(filePath)) {
			if (isUserTmpFile) {
				moveFile(filePath + tmpSuffix, filePath);
			}
			listener.transferred(100);
			listener.completed(filePath);
		}
		return true;
	}

	public static void closeRandomAccessFile(RandomAccessFile out) {
		try {
			out.close();
		} catch (IOException ex) {
		}
	}

	public static void closeOutputStream(OutputStream out) {
		try {
			if (null != out)
				out.close();
		} catch (IOException ex) {
		}
	}

	public static void closeInputStream(InputStream in) {
		try {
			if (null != in)
				in.close();
		} catch (IOException ex) {
		}
	}

	public static boolean writeFile(File file, InputStream io, long size, DataTransferListener listener) throws IOException {
		DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
		try {
			if (listener != null)
				listener.started();

			long downloadSize = 0;
			int nRead = 0;
			byte[] b = new byte[1024 * 64];
			while ((nRead = io.read(b, 0, 1024)) != -1) {
				if (listener == null) {
					out.write(b, 0, nRead);
					downloadSize += nRead;
				} else {
					if (listener.isCanceled(file.getAbsolutePath())) {
						// listener.aborted();
						file.delete();
						return false;
					} else {
						out.write(b, 0, nRead);
						downloadSize += nRead;
						listener.transferred((int) (downloadSize * 100 / size));
					}
				}
			}

			if (listener != null && !listener.isCanceled(file.getAbsolutePath())) {
				listener.transferred(100);
				listener.completed(file.getAbsolutePath());
			}
			return true;
		} finally {
			try {
				out.close();
			} catch (IOException ex) {

			}
		}
	}

	/**
	 * 写入文件
	 * 
	 * @param strFileName
	 *            文件名
	 * @param ins
	 *            流
	 */
	public static boolean writeToFile(String strFileName, InputStream ins) {
		return writeToFile(strFileName, ins, false);
	}

	public static boolean writeToFile(String strFileName, InputStream ins, boolean append) {
		try {
			File file = new File(strFileName);

			FileOutputStream fouts = new FileOutputStream(file, append);
			int len;
			int maxSize = 64 * 1024;
			byte buf[] = new byte[maxSize];
			while ((len = ins.read(buf, 0, maxSize)) != -1) {
				fouts.write(buf, 0, len);
				fouts.flush();
			}

			fouts.close();
			return true;
		} catch (IOException e) {
			MLog.e(TAG, "IOException", e);
			return false;
		}
	}

	/**
	 * 写入文件
	 * 
	 * @param strFileName
	 *            文件名
	 * @param bytes
	 *            bytes
	 */
	public static boolean writeToFile(String strFileName, byte[] bytes) {
		try {
			File file = new File(strFileName);

			FileOutputStream fouts = new FileOutputStream(file);
			fouts.write(bytes, 0, bytes.length);
			fouts.flush();
			fouts.close();
			return true;
		} catch (IOException e) {
			MLog.e(TAG, "IOException", e);
		}
		return false;
	}

	public static boolean writeToFile(String filename, String data) {
		return writeToFile(filename, data, false);
	}

	/**
	 * Prints some data to a file using a BufferedWriter
	 */
	public static boolean writeToFile(String filename, String data, boolean append) {
		BufferedWriter bufferedWriter = null;
		try {
			// Construct the BufferedWriter object
			bufferedWriter = new BufferedWriter(new FileWriter(filename, append));
			// Start writing to the output stream
			bufferedWriter.write(data);
			return true;
		} catch (FileNotFoundException ex) {
			MLog.e(TAG, "FileNotFoundException", ex);
		} catch (IOException ex) {
			MLog.e(TAG, "IOException", ex);
		} finally {
			// Close the BufferedWriter
			try {
				if (bufferedWriter != null) {
					bufferedWriter.flush();
					bufferedWriter.close();
				}
			} catch (IOException ex) {
				MLog.e(TAG, "IOException", ex);
			}
		}
		return false;
	}

	public static String readFile(String filePath) {
		FileInputStream inputstream = null;
		InputStreamReader reader = null;
		String result = "";
		try {
			inputstream = new FileInputStream(filePath);
			StringBuilder builder = new StringBuilder();
			reader = new InputStreamReader(inputstream);
			char buffer[] = new char[1024];
			int count;
			while ((count = reader.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, count);
			}
			result = builder.toString();
		} catch (FileNotFoundException e) {
			MLog.e(TAG, "FileNotFoundException", e);
		} catch (IOException e) {
			MLog.e(TAG, "IOException", e);
		} finally {
			try {
				if (null != inputstream) {
					inputstream.close();
					inputstream = null;
				}
				if (null != reader) {
					reader.close();
					reader = null;
				}
			} catch (IOException e) {
				MLog.e(TAG, "IOException", e);
			}
		}

		return result;
	}

	/**
	 * 新建目录
	 * 
	 * @param folderPath
	 *            String 如 c:/fqf
	 * @return boolean
	 */
	public static boolean newFolder(String folderPath) {
		try {
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.mkdirs();
			}
			return true;
		} catch (Exception e) {
			System.out.println("新建目录操作出错");
			MLog.e(TAG, "Exception", e);
			return false;
		}
	}

	/**
	 * 新建文件
	 * 
	 * @param filePathAndName
	 *            String 文件路径及名称 如c:/fqf.txt
	 * @param fileContent
	 *            String 文件内容
	 * @return boolean
	 */
	public static boolean newFile(String filePathAndName, String fileContent) {
		try {
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			FileWriter resultFile = new FileWriter(myFilePath);
			PrintWriter myFile = new PrintWriter(resultFile);
			String strContent = fileContent;
			myFile.println(strContent);
			myFile.close();
			resultFile.close();
			return true;
		} catch (Exception e) {
			System.out.println("新建目录操作出错");
			MLog.e(TAG, "Exception", e);
			return false;
		}
	}

	/**
	 * 新建文件
	 * 
	 * @param fileFullName
	 * @return
	 * @throws IOException
	 */
	public static void newFile(String fileFullName) throws IOException {
		int pos = StringUtil.getPathLastIndex(fileFullName);
		if (pos > 0) {
			String strFolder = fileFullName.substring(0, pos);
			File file = new File(strFolder);
			if (!file.isDirectory()) {
				file.mkdirs();
			}
		}
		File file = new File(fileFullName);
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
	}

	/**
	 * 递归查找文件
	 * 
	 * @param baseDirName
	 *            查找的文件夹路径
	 * @param targetFileName
	 *            需要查找的文件名 (通配符)
	 * @param fileList
	 *            查找到的文件集合
	 */
	public static void findFiles(String baseDirName, String targetFileName, List<String> fileList) {
		/**
		 * 算法简述： 从某个给定的需查找的文件夹出发，搜索该文件夹的所有子文件夹及文件， 若为文件，则进行匹配，匹配成功则加入结果集，若为子文件夹，则进队列。 队列不空，重复上述操作，队列为空，程序结束，返回结果。
		 */
		String tempName = null;
		// 判断目录是否存在
		File baseDir = new File(baseDirName);
		if (!baseDir.exists() || !baseDir.isDirectory()) {
			return;
		} else {
			String[] filelist = baseDir.list();
			for (int i = 0; i < filelist.length; i++) {
				File readfile = new File(baseDirName + File.separator + filelist[i]);
				if (!readfile.isDirectory()) {
					tempName = readfile.getName();
					if (wildcardMatch(targetFileName, tempName)) {
						// 匹配成功，将文件名添加到结果集
						fileList.add(readfile.getAbsolutePath());
					}
				} else if (readfile.isDirectory()) {
					findFiles(baseDirName + File.separator + filelist[i], targetFileName, fileList);
				}
			}
		}
	}

	/**
	 * 通配符匹配
	 * 
	 * @param pattern
	 *            通配符模式
	 * @param str
	 *            待匹配的字符串
	 * @return 匹配成功则返回true，否则返回false
	 */
	private static boolean wildcardMatch(String pattern, String str) {
		int patternLength = pattern.length();
		int strLength = str.length();
		int strIndex = 0;
		char ch;
		for (int patternIndex = 0; patternIndex < patternLength; patternIndex++) {
			ch = pattern.charAt(patternIndex);
			if (ch == '*') {
				// 通配符星号*表示可以匹配任意多个字符
				while (strIndex < strLength) {
					if (wildcardMatch(pattern.substring(patternIndex + 1), str.substring(strIndex))) {
						return true;
					}
					strIndex++;
				}
			} else if (ch == '?') {
				// 通配符问号?表示匹配任意一个字符
				strIndex++;
				if (strIndex > strLength) {
					// 表示str中已经没有字符匹配?了。
					return false;
				}
			} else {
				if ((strIndex >= strLength) || (ch != str.charAt(strIndex))) {
					return false;
				}
				strIndex++;
			}
		}// for (int patternIndex = 0; patternIndex < patternLength; patternIndex++)

		return (strIndex == strLength);
	}

	public static boolean exist(String filePath) {
		if (null != filePath && filePath.length() > 0) {
			File f = new File(filePath);
			return f.exists();
		}

		return false;
	}

	public static boolean ensureFileExist(String filePath) {
		File f = new File(filePath);
		if (!f.exists()) {
			try {
				FileUtil.newFile(filePath);
				return true;
			} catch (IOException e) {
				MLog.e(TAG, "createLocalRecordPath", e);
				return false;
			}
		}
		return true;
	}

	public static String getRealPath(Context context, Uri uri) {
		String filePath = null;
		String uriString = uri.toString();
		if (uriString.startsWith("content")) {
			String[] filePathColumn = { MediaColumns.DATA };
			Cursor cursor = null;
			try {
				cursor = context.getContentResolver().query(uri, filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				filePath = cursor.getString(columnIndex);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}
		} else if (uriString.startsWith("file")) {
			filePath = uri.getPath();
		}

		return filePath;
	}
	
	public static class IOErrorException extends Exception {

		private static final long serialVersionUID = 1L;

		public IOErrorException() {
			super();
		}

		public IOErrorException(String detailMessage, Throwable throwable) {
			super(detailMessage, throwable);
		}

		public IOErrorException(String detailMessage) {
			super(detailMessage);
		}

		public IOErrorException(Throwable throwable) {
			super(throwable);
		}

	}
}
