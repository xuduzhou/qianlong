package com.delux.qianlong.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.os.Build;

public class NetUtil {

	public static String postForString(String strURL, String strBody, HashMap<String, String> headers) throws IOException {
		return convertStreamToString(post(strURL, strBody, headers));
	}

	public static InputStream post(String strURL, String strBody, HashMap<String, String> headers) throws IOException {
		// Post request
		URL url = new URL(strURL);
		HttpURLConnection connection;
		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			connection = (HttpsURLConnection) url.openConnection();
			((HttpsURLConnection) connection).setHostnameVerifier(DO_NOT_VERIFY);
		} else {
			connection = (HttpURLConnection) url.openConnection();
		}
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		if (headers != null) {
			for (String key : headers.keySet()) {
				connection.setRequestProperty(key, headers.get(key));
			}
		}
		connection.setConnectTimeout(15 * 1000);
		connection.setReadTimeout(15 * 1000);
		if (null != strBody) {
			OutputStream out = connection.getOutputStream();
			out.write(strBody.getBytes());
			out.flush();
			out.close();
		}

		InputStream response = connection.getInputStream();
		// Parse response to XML
		return response;
	}

	public static String getForString(String strURL, HashMap<String, String> headers) throws IOException {
		return convertStreamToString(get(strURL, headers));
	}

	public static InputStream get(String strURL, HashMap<String, String> headers) throws IOException {
		// Post request
		URL url = new URL(strURL);
		HttpURLConnection connection;
		if (url.getProtocol().toLowerCase().equals("https")) {
			trustAllHosts();
			connection = (HttpsURLConnection) url.openConnection();
			((HttpsURLConnection) connection).setHostnameVerifier(DO_NOT_VERIFY);
		} else {
			connection = (HttpURLConnection) url.openConnection();
		}
		connection.setDoInput(true);
		connection.setRequestMethod("GET");
		if (headers != null) {
			for (String key : headers.keySet()) {
				connection.setRequestProperty(key, headers.get(key));
			}
		}
		connection.setConnectTimeout(15 * 1000);
		connection.setReadTimeout(15 * 1000);
		InputStream response = connection.getInputStream();
		// Parse response to XML
		return response;

	}

	// always verify the host - dont check for certificate
	final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server - dont check for any certificate
	 */
	private static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {
				// TODO Auto-generated method stub
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static boolean isUsingNetwork(Context context) {
		if (context == null)// for bug:180170
		{
			return false;
		}
		boolean isUsingNetwork = false;
		try {
			WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			if (wifiManager.isWifiEnabled()) {
				isUsingNetwork = isWifiConnected(context); // 检查wifi
			}
			if (isMobileConnected(context)) // 检查移动网络
			{
				isUsingNetwork = true;
			}
			if (Build.VERSION.SDK_INT >= 13) {
				if (!isUsingNetwork && isEthernetConnected(context)) { // 检查有线网络
					isUsingNetwork = true;
				}
			}
		} catch (Exception e) {
			// 调用远程服务有可能遇到异常
			MLog.e("NetUtil", "check using network error.", e);
		}
		return isUsingNetwork;
	}

	public static boolean isWifiConnected(Context context) {
		State state = getNetworkState(context, ConnectivityManager.TYPE_WIFI);
		return state == State.CONNECTED || state == State.CONNECTING;
	}

	public static boolean isMobileConnected(Context context) {
		State state = getNetworkState(context, ConnectivityManager.TYPE_MOBILE);
		return state == State.CONNECTED || state == State.CONNECTING;
	}

	public static boolean isEthernetConnected(Context context) {
		State state = getNetworkState(context, 9); // API Level 13以上才支持ConnectivityManager.TYPE_ETHERNET = 9
		return state == State.CONNECTED || state == State.CONNECTING;
	}

	public static State getNetworkState(Context context, int networkType) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = cm.getNetworkInfo(networkType);
			return info == null ? null : info.getState();
		} catch (Exception e) {
			// 可能报出ANR异常, 此处捕获处理.
			return null;
		}

		/*
		 * ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); NetworkInfo info = cm.getNetworkInfo(networkType); return info == null ? null : info.getState();
		 */
	}

	public static interface DownloadCallback {
		void onBegin(int fileSize);

		void onFinish(boolean result);

		void onProgressUpdate(int downloadSize);

		void onException(Exception e);

		void onCancel();
	}

	public static class DownloadCallbackAdapter implements DownloadCallback {
		@Override
		public void onBegin(int fileSize) {
		}

		@Override
		public void onFinish(boolean result) {
		}

		@Override
		public void onProgressUpdate(int downloadSize) {
		}

		@Override
		public void onException(Exception e) {
		}

		@Override
		public void onCancel() {
		}
	}

	public static class FileDownloader {
		DownloadCallback mCallback;
		boolean isCanceled;
		int mDownloadSize;

		public FileDownloader(DownloadCallback callback) {
			super();
			mCallback = callback;
		}

		public void cancel() {
			isCanceled = true;
		}

		public void download(String url, String downloadPath) {
			if (null == url || null == downloadPath) {
				return;
			}

			HttpURLConnection conn = null;
			InputStream input = null;
			RandomAccessFile oSavedFile = null;
			isCanceled = false;
			mDownloadSize = 0;
			try {
				conn = (HttpURLConnection) (new URL(url)).openConnection();
				// for bug:142345 默认情况下，HttpURLConnection 使用 gzip方式获取, 要求http请求不要gzip压缩
				conn.setRequestProperty("Accept-Encoding", "identity");
				int fileSize = conn.getContentLength();// 根据响应获取文件大小
				mCallback.onBegin(fileSize);
				input = conn.getInputStream();

				// 获得输入流
				long nPos = 0;
				File file = new File(downloadPath);
				if (file.exists())
					file.delete();

				file.createNewFile();
				oSavedFile = new RandomAccessFile(file, "rw");
				// 定位文件指针到nPos位置
				oSavedFile.seek(nPos);
				byte[] b = new byte[1024];
				int nRead;
				// 从输入流中读入字节流，然后写到文件中
				while ((nRead = input.read(b, 0, 1024)) > 0) {
					if (isCanceled) {
						if (null != mCallback)
							mCallback.onCancel();
						oSavedFile.close();
						return;
					}

					mDownloadSize += nRead;
					oSavedFile.write(b, 0, nRead);
					mCallback.onProgressUpdate(mDownloadSize);
				}
				mCallback.onFinish(true);
			} catch (Exception e) {
				MLog.e("NetUtil", e.getMessage());
				mCallback.onException(e);
			} finally {
				try {
					if (input != null)
						input.close();

					if (conn != null)
						conn.disconnect();

					if (oSavedFile != null)
						oSavedFile.close();

				} catch (IOException e) {
					e.printStackTrace();
					mCallback.onException(e);
				}
			}
		}
	}

	public static boolean checkNetwork(Context context) {
		if (!NetUtil.isUsingNetwork(context)) {
			return false;
		}

		return true;
	}

}
