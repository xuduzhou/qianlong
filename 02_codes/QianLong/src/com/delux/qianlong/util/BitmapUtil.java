package com.delux.qianlong.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

public class BitmapUtil {

	/**
	 * 压缩图片
	 * 
	 * @param fileName
	 *            图片路径
	 * @param dstSize
	 *            压缩后大小（最长边）
	 * @param format
	 * @return
	 */
	public static boolean scaleBitmap(String fileName, int dstSize) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(fileName, options);

		options.inJustDecodeBounds = false;
		if (options.outWidth > options.outHeight) {
			options.inSampleSize = options.outWidth / dstSize;
		} else {
			options.inSampleSize = options.outHeight / dstSize;
		}

		Bitmap bitmap = BitmapFactory.decodeFile(fileName, options);
		bitmap = Bitmap.createScaledBitmap(bitmap, dstSize, dstSize * bitmap.getHeight() / bitmap.getWidth(), true);
		try {
			bitmap.compress(CompressFormat.JPEG, 100, new FileOutputStream(fileName));
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} finally {
			bitmap = null;
		}
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static Bitmap decodeSampledBitmapFromFile(String filePath, int resId, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(filePath, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	/**
	 * 保存图片到sdcard
	 * 
	 * @param imageStr
	 * @return
	 */
	public static boolean writ2SD(final String imageStr,final Context context,final Handler handler) {
		
		Thread thread = new Thread(new Runnable() {
			
			private File fileName;

			@Override
			public void run() {
				// TODO Auto-generated method stub
				InputStream in = null;
				FileOutputStream fileOutputStream = null;
				HttpURLConnection con = null;
				if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
					File fileDirectory = Environment.getExternalStorageDirectory();
					
					try {
						if (!fileDirectory.exists()) {
							fileDirectory.mkdirs();
						}
						 fileName = new File(fileDirectory, subStringAsName(imageStr));
						if (!fileName.exists()) {
							fileName.createNewFile();
						}

						URL imageUrl = new URL(imageStr);
						con = (HttpURLConnection) imageUrl.openConnection();
						con.connect();

						in = con.getInputStream();
						fileOutputStream = new FileOutputStream(fileName);
						byte[] b = new byte[1024];
						int temp = 0;
						while ((temp = in.read(b)) != -1) {
							fileOutputStream.write(b, 0, temp);
						}
					} catch (Exception e) {
						e.printStackTrace();
						handler.sendEmptyMessage(-1);
						return;
					} finally {
						try {
							if (in != null) {
								in.close();
							}
						} catch (Exception e) {
							e.printStackTrace();
							handler.sendEmptyMessage(-1);
							return;
						}
						try {
							if (fileOutputStream != null) {
								fileOutputStream.close();
							}
						} catch (Exception e) {
							e.printStackTrace();
							handler.sendEmptyMessage(-1);
							return;
						}
						try {
							if (con != null) {
								con.disconnect();
							}
						} catch (Exception e) {
							e.printStackTrace();
							handler.sendEmptyMessage(-1);
							return;
						}
					}
				}else{
					
					handler.sendEmptyMessage(2);
					return;
				}
				Message msg = new Message();
				msg.what=3;
				msg.obj=fileName.getPath();
				System.out.println("filepath  "+fileName.getPath());
				handler.sendMessage(msg);
				
			}
		});
		
		thread.start();
		
		return true;
	}

	private static String subStringAsName(String strUrl) {
		int index = strUrl.lastIndexOf("/");
		int index1 = strUrl.lastIndexOf("?");
		String name = strUrl.substring(index,index1);
		return name;
	}

}
