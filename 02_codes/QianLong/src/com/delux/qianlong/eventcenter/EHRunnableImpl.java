package com.delux.qianlong.eventcenter;

import com.delux.qianlong.eventcenter.EventHandler.EHRunnable;

/**
 * EC(继承此类,复写pushEventName和run方法)
 * 
 * @author xiongwen
 * 
 */
public abstract class EHRunnableImpl implements EHRunnable {
	public abstract EventName pushEventName();

	public EHRunnableImpl() {
		EventHandler.getInstence().regedit(pushEventName(), this);
	}
}
