package com.delux.qianlong.eventcenter;

import java.util.HashMap;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * EC跨模块调用(EventHandler.getInstance().dispatch(...))
 * 
 * @author xiongwen
 * 
 */
public class EventHandler {
	private static EventHandler eh;
	private HashMap<EventName, EHRunnable> eventList = new HashMap<EventName, EHRunnable>();
	private Handler handler = new Handler(Looper.getMainLooper()) { // main
																	// looper
		@Override
		public void handleMessage(Message msg) {
			dispatchSyncIntenal(null, EventName.values()[msg.arg1], (Object[]) msg.obj);
		}
	};

	public static EventHandler getInstence() {
		if (eh == null) {
			eh = new EventHandler();
		}
		return eh;
	}

	public void regedit(EventName eventName, EHRunnable run) {
		eventList.put(eventName, run);
	}

	public void unRegedit(EventName eventName) {
		if (eventList.get(eventName) == null) {
			return;
		}
		eventList.remove(eventName);
	}

	/*
	 * 异步调用，采用handler模式，跳出当前函数栈执行runable,runable在主ui线程执行
	 */
	public void dispatch(EventName eventName, Object... inValues) {
		Message msg = handler.obtainMessage();
		msg.what = 1;
		// 可变参数直接转换为数组，但不可逆，将可变参数全部转化为数组进行传递
		Object[] inVal = inValues;
		msg.obj = inVal;
		msg.arg1 = eventName.ordinal();
		msg.sendToTarget();
	}

	/*
	 * 同步调用，直接在当前执行栈执行runable
	 */
	public void dispatchSync(EventName eventName, Object... inValues) {
		dispatchSync(null, eventName, inValues);
	}

	/*
	 * 带返回值的 同步调用，直接在当前执行栈执行runable
	 */
	public void dispatchSync(Object[] outValues, EventName eventName, Object... inValues) {
		dispatchSyncIntenal(outValues, eventName, inValues);
	}

	private void dispatchSyncIntenal(Object[] outValues, EventName eventName, Object[] inValues) {
		EHRunnable ecRunnable = eventList.get(eventName);
		if (ecRunnable == null) {
			return;
		}
		ecRunnable.run(outValues, inValues);
	}

	public void onDestroy() {
		handler.removeMessages(1);
		eventList.clear();
		eh = null;
	}

	public void post(Runnable runnable) {
		handler.post(runnable);
	}

	public void postDelay(Runnable runnable, long delayMillis) {
		handler.postDelayed(runnable, delayMillis);
	}

	/*
	 * runable interface
	 */
	public interface EHRunnable {
		public void run(Object[] outValues, Object[] inValues);
	}
}
