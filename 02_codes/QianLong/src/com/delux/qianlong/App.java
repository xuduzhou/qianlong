package com.delux.qianlong;

import android.app.Application;

import com.delux.qianlong.util.MToast;

public class App extends Application {
	
	private static App mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		
		mInstance = this;
		MToast.init(this);
	}
	
	public static App getInstance() {
		return mInstance;
	}
	
}
