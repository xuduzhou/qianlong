package com.delux.qianlong.net;

public enum NetException {

	/**
	 * 服务器返回失败状态
	 */
	ServerStatusFailException,

	/**
	 * 解析json数据异常
	 */
	JSONException,

	/**
	 * 取消
	 */
	CancelException,

	/**
	 * 请求异常(服务器未成功响应)
	 */
	RequestException,
	
	/**
	 * 上传的文件不存在
	 */
	FileNotFoundException,

}
