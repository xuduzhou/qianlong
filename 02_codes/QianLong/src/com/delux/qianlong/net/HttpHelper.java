package com.delux.qianlong.net;

import com.loopj.android.http.AsyncHttpClient;

public class HttpHelper {

	private static HttpHelper mInstance;
	private AsyncHttpClient mAsyncHttpClient;
	private IRequest mRequest;

	private HttpHelper() {
		mAsyncHttpClient = new AsyncHttpClient();
		mAsyncHttpClient.setTimeout(30);
		mRequest = new IRequestImpl();
	}

	private static HttpHelper getInstance() {
		if (null == mInstance) {
			mInstance = new HttpHelper();
		}
		return mInstance;
	}

	public static IRequest getHttpRequest() {
		return getInstance().getRequest();
	}

	public static AsyncHttpClient getHttpClient() {
		return getInstance().mAsyncHttpClient;
	}

	private IRequest getRequest() {
		return mRequest;
	}

}
