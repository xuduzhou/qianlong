package com.delux.qianlong.net;


/**
 * 请求接口
 *
 * @author xiongwen
 *
 */
public interface IRequest {

	String BASE_URL = "";

	void post(BaseRequestParams params, BaseResponseHandler responseHandler);

}
