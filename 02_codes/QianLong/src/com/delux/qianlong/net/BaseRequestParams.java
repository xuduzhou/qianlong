package com.delux.qianlong.net;

import com.loopj.android.http.RequestParams;

/**
 * 请求参数基类
 * 
 * @author xiongwen
 * 
 */
public class BaseRequestParams extends RequestParams {

	private static final long serialVersionUID = 1L;
	private String mUrl;

	public BaseRequestParams(String url) {
		mUrl = url;
	}

	public String getUrl() {
		return mUrl;
	}

}
