package com.delux.qianlong.net;


/**
 * 请求接口实现类
 * 
 * @author xiongwen
 * 
 */
public class IRequestImpl implements IRequest {

	private String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}

	@Override
	public void post(BaseRequestParams params, BaseResponseHandler responseHandler) {
		HttpHelper.getHttpClient().post(getAbsoluteUrl(params.getUrl()), params, responseHandler);
	}

}
