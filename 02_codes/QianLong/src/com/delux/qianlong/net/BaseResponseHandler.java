package com.delux.qianlong.net;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.delux.qianlong.App;
import com.delux.qianlong.AppConfig;
import com.delux.qianlong.R;
import com.delux.qianlong.util.MLog;
import com.loopj.android.http.JsonHttpResponseHandler;

/**
 * 请求返回基类
 * 
 * @author xiongwen
 * 
 */
public abstract class BaseResponseHandler extends JsonHttpResponseHandler {

	private static final String TAG = AppConfig.DEBUG ? BaseResponseHandler.class.getSimpleName() : null;

	public static final String KEY_DATA = "data";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_STATUS = "status";
	public static final int STATUS_SUCCESS = 200;
	public static final int STATUS_LOGOUT = 101;

	@Override
	public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
		super.onSuccess(statusCode, headers, response);
		try {
			JSONObject data = response.getJSONObject(KEY_DATA);
			int status = data.getInt(KEY_STATUS);
			if (STATUS_SUCCESS == status) {
				onSuccess(data);
			} else {
				String message = data.getString(KEY_MESSAGE);
				onFail(NetException.ServerStatusFailException, status, message);
			}
		} catch (JSONException e) {
			onFail(NetException.JSONException, 0, App.getInstance().getString(R.string.network_error));
		}
	}

	@Override
	public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
		MLog.e(TAG, "onFailure: statusCode" + statusCode + "," + responseString);
		onFail(NetException.RequestException, statusCode, App.getInstance().getString(R.string.network_error));
	}

	@Override
	public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
		MLog.e(TAG, "onFailure: statusCode" + statusCode + "," + errorResponse);
		onFail(NetException.RequestException, statusCode, App.getInstance().getString(R.string.network_error));
	}

	@Override
	public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
		MLog.e(TAG, "onFailure: statusCode" + statusCode + "," + errorResponse);
		onFail(NetException.RequestException, statusCode, App.getInstance().getString(R.string.network_error));
	}

	@Override
	public void onCancel() {
		MLog.e(TAG, "Request got cancelled");
		onFail(NetException.CancelException, 0, "cancel");
	}

	protected abstract void onSuccess(JSONObject response);

	protected abstract void onFail(NetException exception, int status, String message);

}
