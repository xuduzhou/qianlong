package com.delux.qianlong.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.LayoutInflater;
import android.view.View;

import com.delux.qianlong.R;
import com.delux.qianlong.thread.KThreadUtil;

public class CircleProgress {

	private static Dialog mDialog;

	private static void initDialog(Context context) {
		if (null == mDialog) {
			mDialog = new Dialog(context, R.style.public_translucent_fullscreen_dialog);
			View view = LayoutInflater.from(context).inflate(R.layout.circle_progress, null);
			mDialog.setContentView(view);
			mDialog.setOnDismissListener(new OnDismissListener() {
				
				@Override
				public void onDismiss(DialogInterface dialog) {
					dismiss();
				}
			});
		}
	}

	public static void show(final Context context) {
		show(context, true);
	}

	public static void show(final Context context, final boolean cancelable) {
		KThreadUtil.runInUiThread(new Runnable() {

			@Override
			public void run() {
				initDialog(context);
				mDialog.setCancelable(cancelable);
				mDialog.show();
			}
		}, false);
	}

	public static void dismiss() {
		KThreadUtil.runInUiThread(new Runnable() {

			@Override
			public void run() {
				if (null != mDialog) {
					mDialog.dismiss();
					mDialog = null;
				}
			}
		}, false);
	}

}
