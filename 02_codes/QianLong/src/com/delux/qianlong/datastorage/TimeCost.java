package com.delux.qianlong.datastorage;

import com.classletter.common.log.MLog;

/**
 * 耗时日志打印
 */
public class TimeCost {
	public static void plog(String tag, long timeStart, String msg) {
		MLog.d(tag, "[performance] " + (System.currentTimeMillis() - timeStart) + "ms to " + msg);
	}
}
