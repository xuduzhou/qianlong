package com.delux.qianlong.datastorage;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 持久化存储接口
 * 
 * @author XiongWen
 * 
 */
public interface IPersistent {
	// ============public存储 =============

	boolean putBoolean(IPersistentPublicKeys key, boolean value);

	boolean putFloat(IPersistentPublicKeys key, float value);

	boolean putInt(IPersistentPublicKeys key, int value);

	boolean putLong(IPersistentPublicKeys key, long value);

	boolean putString(IPersistentPublicKeys key, String value);

	boolean remove(IPersistentPublicKeys key);

	boolean contains(IPersistentPublicKeys key);

	boolean getBoolean(IPersistentPublicKeys key, boolean defValue);

	float getFloat(IPersistentPublicKeys key, float defValue);

	int getInt(IPersistentPublicKeys key, int defValue);

	long getLong(IPersistentPublicKeys key, long defValue);

	String getString(IPersistentPublicKeys key, String defValue);

	// ============public存储end =============

	// ============模块私有存储=============

	boolean containsModel(String fileName, String key);

	<T extends DataModel> boolean putModel(String fileName, String key, T model);

	<T extends DataModel> boolean putModels(String fileName, String key, ArrayList<T> models);

	<K, T extends DataModel> boolean putHashModels(String fileName, String key, HashMap<K, T> models);

	<T extends DataModel> T getModel(String fileName, String key);

	<T extends DataModel> ArrayList<T> getModels(String fileName, String key);

	<K, T extends DataModel> HashMap<K, T> getHashModels(String fileName, String key);

	boolean removeModel(String fileName, String key);
	
	boolean removeModel(String fileName);

	// ============私有存储end============

}
