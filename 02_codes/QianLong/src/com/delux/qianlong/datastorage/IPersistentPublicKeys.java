package com.delux.qianlong.datastorage;

/**
 * 持久化数据key接口
 * 
 * @author XiongWen
 * 
 */
public interface IPersistentPublicKeys {
	public String getString();
}
