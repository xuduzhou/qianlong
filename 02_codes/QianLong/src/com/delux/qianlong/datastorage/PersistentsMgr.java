package com.delux.qianlong.datastorage;

/**
 * 持久化存储调度类 上层请调用通过该接口, 利于后期扩展或切换
 * 
 * @author XiongWen
 * 
 */
public class PersistentsMgr {
	public enum PersistentsType {
		SP, // SharedPreferences
		// TODO other such as custom file stroage
	}

	/*
	 * 默认SharedPreferences(文件)持久化存储器
	 */
	public static IPersistent get() {
		return get(PersistentsType.SP);
	}

	public static IPersistent get(PersistentsType type) {
		IPersistent persistent = null;
		switch (type) {
		case SP:
			persistent = SPPersistent.getInstance();
			break;
		}
		return persistent;
	}
}
