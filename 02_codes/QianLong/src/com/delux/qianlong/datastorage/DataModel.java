package com.delux.qianlong.datastorage;

import java.io.Serializable;

/**
 * 数据模型api 注： 所有继承model的子类不能被混淆
 * 
 * @author XiongWen
 * 
 */
public interface DataModel extends Serializable {
	// private static final long serialVersionUID = 1L;
}
