package com.delux.qianlong.datastorage;

/**
 * 持久化 public存储 keys 统一类，采用枚举的名字作为数据保存key (防止多个模块定义相同的key值, 造成数据混乱)
 * 
 * @author XiongWen
 * 
 */
public enum PersistentPublicKeys implements IPersistentPublicKeys {

	;
	@Override
	public String getString() {
		return this.name();
	}
}
