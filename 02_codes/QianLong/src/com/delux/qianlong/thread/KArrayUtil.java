package com.delux.qianlong.thread;

/**
 * 线程相关类,不建议直接调用
 * 
 * @author XiongWen
 * 
 */
public class KArrayUtil {

	private static final int VECTOR_INC_MIN_STEP = 5;

	public static int idealIntArraySize(int size) {
		return size + Math.max(VECTOR_INC_MIN_STEP, size >> 2);
	}
}
