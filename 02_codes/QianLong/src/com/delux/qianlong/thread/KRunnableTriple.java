package com.delux.qianlong.thread;

/**
 * 线程相关类,不建议直接调用
 * 
 * @author XiongWen
 * 
 */
public class KRunnableTriple implements Runnable {

	private static KRunnableTriple _listHead;
	private KRunnableTriple _next;

	private Runnable first;
	private Runnable second;
	private Runnable third;

	synchronized static public KRunnableTriple obtain(Runnable first, Runnable second, Runnable third) {
		KRunnableTriple runnable = _listHead;
		if (null == runnable)
			runnable = new KRunnableTriple();
		else
			_listHead = runnable._next;

		runnable.first = first;
		runnable.second = second;
		runnable.third = third;
		runnable._next = null;
		return runnable;
	}

	synchronized static public KRunnableTriple obtain(Runnable first, Runnable second) {
		return obtain(first, second, null);
	}

	synchronized static private void recycle(KRunnableTriple runnable) {
		runnable.first = null;
		runnable.second = null;
		runnable.third = null;
		runnable._next = _listHead;
		_listHead = runnable;
	}

	@Override
	public void run() {
		if (null != first)
			first.run();
		if (null != second)
			second.run();
		if (null != third)
			third.run();
		recycle(this);
	}
}
