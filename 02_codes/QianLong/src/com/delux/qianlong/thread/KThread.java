package com.delux.qianlong.thread;

import android.os.Handler;
import android.os.Looper;

/**
 * 线程类
 * 
 * @author XiongWen
 * 
 */
public class KThread implements Runnable {

	static final boolean DEBUG = false; // DebugConfig.DEBUG;

	static final String TAG = KThread.class.getSimpleName();

	volatile int _index;
	volatile boolean _droped;

	private Handler mHandler;

	private volatile boolean mIsQuiting;

	private static final int STARTED = 1 << 0;
	private static final int WORKING = 1 << 2;
	private static final int QUITED = 1 << 4;
	private volatile int mStatus;

	private Thread mThead = new Thread(this);

	final public Handler getHandler() {
		return mHandler;
	}

	final public boolean isQuiting() {
		return mIsQuiting;
	}

	final public void recycle() {
		mThead.setName("");
		KThreadPool.joinThreadPool(this);
	}

	private KThread() {

	}

	static KThread createThread() {
		KThread thr = new KThread();
		thr.mThead.start();

		synchronized (thr) {
			while (!((thr.mStatus & STARTED) == STARTED)) {
				try {
					thr.wait();
				} catch (InterruptedException e) {
				}
			}
		}

		return thr;
	}

	final void waitQuit() {
		synchronized (this) {
			while (!((mStatus & QUITED) == QUITED)) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
		}
	}

	final boolean isQuited() {
		return (mStatus & QUITED) == QUITED;
	}

	public final void waitWorkDone() {
		synchronized (this) {
			while ((mStatus & WORKING) == WORKING) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
		}
	}

	@Override
	final public void run() {
		Looper.prepare();

		mHandler = new Handler();

		synchronized (this) {
			mStatus |= STARTED;
			notifyAll();
		}

		Looper.loop();

		synchronized (this) {
			mStatus |= QUITED;
			notifyAll();
		}
	}

	final void quit() {
		quit(false);
	}

	final void quit(boolean mayInterruptIfRunning) {
		if (isQuiting()) {
			return;
		}
		mIsQuiting = true;
		mHandler.post(new Runnable() {
			public void run() {
				Looper.myLooper().quit();
			}
		});
		if (mayInterruptIfRunning) {
			interrupt();
		}
	}

	public final void interrupt() {
		mThead.interrupt();
	}

	private void innerExecute(Runnable runnable) {
		innerExecute(runnable, 0);
	}

	private void innerExecute(Runnable runnable, long delayTime) {
		if (isQuiting()) {
			return;
		}
		if (delayTime > 0) {
			mHandler.postDelayed(runnable, delayTime);
		} else {
			mHandler.post(runnable);
		}
	}

	private class RunnableWraper implements Runnable {
		private final Runnable mRunnable;
		private final boolean mRecycle;

		RunnableWraper(Runnable runnable, boolean recycle) {
			mRunnable = runnable;
			mRecycle = recycle;
		}

		@Override
		public void run() {

			synchronized (KThread.this) {
				mStatus |= WORKING;
				KThread.this.notifyAll();
			}

			mRunnable.run();

			synchronized (KThread.this) {
				mStatus &= ~WORKING;
				KThread.this.notifyAll();
			}

			if (mRecycle) {
				recycle();
			}
		}
	}

	public final void execute(Runnable runnable) {
		innerExecute(new RunnableWraper(runnable, false));
	}

	final void autoJoinExecute(Runnable runnable) {
		autoJoinExecute(runnable, 0);
	}

	public void setName(final String name) {
		mThead.setName(name);
	}

	final void autoJoinExecute(Runnable runnable, long delayTime) {
		innerExecute(new RunnableWraper(runnable, true), delayTime);
	}

	public static KThread obtain() {
		return KThreadPool.obtainThread();
	}

	public static void threadExecute(Runnable runnable) {
		threadExecute(runnable, 0);
	}

	public static void threadExecute(Runnable runnable, long delayTime) {
		KThreadPool.threadExecute(runnable, delayTime);
	}

	public static void sleep(int time) throws InterruptedException {
		Thread.sleep(time);
	}

	public static void sleep(long millis, int nanos) throws InterruptedException {
		Thread.sleep(millis, nanos);
	}
}
